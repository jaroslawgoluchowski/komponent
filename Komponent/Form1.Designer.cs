﻿namespace Komponent
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.minBox = new System.Windows.Forms.TextBox();
            this.maxBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.valueBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tickBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.colorComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.miernik1 = new Komponent.Miernik();
            this.miernik2 = new Komponent.Miernik();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(380, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Minimalna wartość :";
            // 
            // minBox
            // 
            this.minBox.Location = new System.Drawing.Point(486, 59);
            this.minBox.Name = "minBox";
            this.minBox.Size = new System.Drawing.Size(164, 20);
            this.minBox.TabIndex = 2;
            // 
            // maxBox
            // 
            this.maxBox.Location = new System.Drawing.Point(486, 85);
            this.maxBox.Name = "maxBox";
            this.maxBox.Size = new System.Drawing.Size(164, 20);
            this.maxBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(368, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Maksymalna wartość :";
            // 
            // valueBox
            // 
            this.valueBox.Location = new System.Drawing.Point(486, 111);
            this.valueBox.Name = "valueBox";
            this.valueBox.Size = new System.Drawing.Size(164, 20);
            this.valueBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(366, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Pokazywana wartość :";
            // 
            // tickBox
            // 
            this.tickBox.Location = new System.Drawing.Point(486, 137);
            this.tickBox.Name = "tickBox";
            this.tickBox.Size = new System.Drawing.Size(164, 20);
            this.tickBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(395, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ilośc podziałek :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(388, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Kolor wskazówki :";
            // 
            // colorComboBox
            // 
            this.colorComboBox.FormattingEnabled = true;
            this.colorComboBox.Items.AddRange(new object[] {
            "Czerwony",
            "Niebieski",
            "Szary",
            "Zielony",
            "Magenta",
            "Fioletowy",
            "Zółty"});
            this.colorComboBox.Location = new System.Drawing.Point(487, 166);
            this.colorComboBox.Name = "colorComboBox";
            this.colorComboBox.Size = new System.Drawing.Size(163, 21);
            this.colorComboBox.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(435, 228);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "START";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // miernik1
            // 
            this.miernik1.BackgroundColor = System.Drawing.Color.Gold;
            this.miernik1.Location = new System.Drawing.Point(12, 12);
            this.miernik1.MajorTicks = 5F;
            this.miernik1.MajorTicksColor = System.Drawing.Color.DarkRed;
            this.miernik1.MaxValue = 10F;
            this.miernik1.MeasuredValue = "oC";
            this.miernik1.MinorTicks = 5;
            this.miernik1.MinorTicksColor = System.Drawing.Color.DarkViolet;
            this.miernik1.MinValue = -10F;
            this.miernik1.Name = "miernik1";
            this.miernik1.NeedleColor = System.Windows.Forms.AGaugeNeedleColor.Blue;
            this.miernik1.NumbersColor = System.Drawing.Color.Black;
            this.miernik1.Size = new System.Drawing.Size(219, 211);
            this.miernik1.TabIndex = 0;
            this.miernik1.Value = 10F;
            // 
            // miernik2
            // 
            this.miernik2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.miernik2.Location = new System.Drawing.Point(67, 305);
            this.miernik2.MajorTicks = 50F;
            this.miernik2.MajorTicksColor = System.Drawing.Color.Black;
            this.miernik2.MaxValue = 200F;
            this.miernik2.MeasuredValue = "V";
            this.miernik2.MinorTicks = 9;
            this.miernik2.MinorTicksColor = System.Drawing.Color.Gray;
            this.miernik2.MinValue = -100F;
            this.miernik2.Name = "miernik2";
            this.miernik2.NeedleColor = System.Windows.Forms.AGaugeNeedleColor.Gray;
            this.miernik2.NumbersColor = System.Drawing.Color.Black;
            this.miernik2.Size = new System.Drawing.Size(219, 195);
            this.miernik2.TabIndex = 12;
            this.miernik2.Value = 0F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 598);
            this.Controls.Add(this.miernik2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.colorComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tickBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.valueBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.maxBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.minBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.miernik1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Miernik miernik1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox minBox;
        private System.Windows.Forms.TextBox maxBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox valueBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tickBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox colorComboBox;
        private System.Windows.Forms.Button button1;
        private Miernik miernik2;
    }
}


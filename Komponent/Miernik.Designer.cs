﻿namespace Komponent
{
    partial class Miernik
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.gauge = new System.Windows.Forms.AGauge();
            this.typeBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // gauge
            // 
            this.gauge.BaseArcColor = System.Drawing.Color.Gray;
            this.gauge.BaseArcRadius = 80;
            this.gauge.BaseArcStart = 135;
            this.gauge.BaseArcSweep = 270;
            this.gauge.BaseArcWidth = 2;
            this.gauge.Center = new System.Drawing.Point(100, 100);
            this.gauge.Location = new System.Drawing.Point(0, 0);
            this.gauge.MaxValue = 200F;
            this.gauge.MinValue = -100F;
            this.gauge.Name = "gauge";
            this.gauge.NeedleColor1 = System.Windows.Forms.AGaugeNeedleColor.Gray;
            this.gauge.NeedleColor2 = System.Drawing.Color.DimGray;
            this.gauge.NeedleRadius = 80;
            this.gauge.NeedleType = System.Windows.Forms.NeedleType.Simple;
            this.gauge.NeedleWidth = 2;
            this.gauge.ScaleLinesInterColor = System.Drawing.Color.Black;
            this.gauge.ScaleLinesInterInnerRadius = 73;
            this.gauge.ScaleLinesInterOuterRadius = 80;
            this.gauge.ScaleLinesInterWidth = 1;
            this.gauge.ScaleLinesMajorColor = System.Drawing.Color.Black;
            this.gauge.ScaleLinesMajorInnerRadius = 70;
            this.gauge.ScaleLinesMajorOuterRadius = 80;
            this.gauge.ScaleLinesMajorStepValue = 50F;
            this.gauge.ScaleLinesMajorWidth = 2;
            this.gauge.ScaleLinesMinorColor = System.Drawing.Color.Gray;
            this.gauge.ScaleLinesMinorInnerRadius = 75;
            this.gauge.ScaleLinesMinorOuterRadius = 80;
            this.gauge.ScaleLinesMinorTicks = 9;
            this.gauge.ScaleLinesMinorWidth = 1;
            this.gauge.ScaleNumbersColor = System.Drawing.Color.Black;
            this.gauge.ScaleNumbersFormat = null;
            this.gauge.ScaleNumbersRadius = 95;
            this.gauge.ScaleNumbersRotation = 0;
            this.gauge.ScaleNumbersStartScaleLine = 0;
            this.gauge.ScaleNumbersStepScaleLines = 1;
            this.gauge.Size = new System.Drawing.Size(218, 209);
            this.gauge.TabIndex = 0;
            this.gauge.Text = "aGauge1";
            this.gauge.Value = 50F;
            // 
            // typeBox
            // 
            this.typeBox.Location = new System.Drawing.Point(182, 0);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(37, 20);
            this.typeBox.TabIndex = 1;
            // 
            // Miernik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.gauge);
            this.Name = "Miernik";
            this.Size = new System.Drawing.Size(219, 195);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.AGauge gauge;
        private System.Windows.Forms.TextBox typeBox;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Komponent
{
    public partial class Miernik : UserControl
    {

        public Miernik()
        {
            InitializeComponent();
        }

        public float Value
        {
            get{
                return (gauge.Value);
            }
            set{
                gauge.Value = value;
            }
        }

        public float MinValue
        {
            get{
                return (gauge.MinValue);
            }
            set{
                gauge.MinValue = value;
            }
        }

        public float MaxValue
        {
            get
            {
                return (gauge.MaxValue);
            }
            set
            {
                gauge.MaxValue = value;
            }
        }

        public int MinorTicks
        {
            get
            {
                return (gauge.ScaleLinesMinorTicks);
            }
            set
            {
                gauge.ScaleLinesMinorTicks = value;
            }
        }

        public float MajorTicks
        {
            get
            {
                return (gauge.ScaleLinesMajorStepValue);
            }
            set
            {
                gauge.ScaleLinesMajorStepValue = value;
            }
        }

        public AGaugeNeedleColor NeedleColor
        {
            get
            {
                return (gauge.NeedleColor1);
            }
            set
            {
                gauge.NeedleColor1 = value;
            }
        }

        public Color BackgroundColor
        {
            get
            {
                return (gauge.BackColor);
            }
            set
            {
                gauge.BackColor = value;
            }
        }

        public Color MinorTicksColor
        {
            get
            {
                return (gauge.ScaleLinesMinorColor);
            }
            set
            {
                gauge.ScaleLinesMinorColor = value;
            }
        }

        public Color MajorTicksColor
        {
            get
            {
                return (gauge.ScaleLinesMajorColor);
            }
            set
            {
                gauge.ScaleLinesMajorColor = value;
            }
        }

        public Color NumbersColor
        {
            get
            {
                return (gauge.ScaleNumbersColor);
            }
            set
            {
                gauge.ScaleNumbersColor = value;
            }
        }

        public String MeasuredValue
        {
            get
            {
                return (typeBox.Text);
            }
            set
            {
                typeBox.Text = value;
            }
        }

        public void setCursorColor(AGaugeNeedleColor color)
        {
            gauge.NeedleColor1 = color;
        }
    }
}

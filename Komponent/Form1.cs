﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Komponent
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if( !String.IsNullOrEmpty(minBox.Text))
            {
                miernik1.MinValue = (int.Parse(minBox.Text));
            }
            if (!String.IsNullOrEmpty(maxBox.Text))
            {
                miernik1.MaxValue = (int.Parse(maxBox.Text));
            }
            if (!String.IsNullOrEmpty(valueBox.Text))
            {
                miernik1.Value = (int.Parse(valueBox.Text));
            }
            if (!String.IsNullOrEmpty(tickBox.Text))
            {
                miernik1.MinorTicks = (int.Parse(tickBox.Text));
            }
           
            switch (colorComboBox.Text)
            {
                case "Niebieski" : miernik1.setCursorColor(AGaugeNeedleColor.Blue); break;
                case "Zółty": miernik1.setCursorColor(AGaugeNeedleColor.Yellow); break;
                case "Szary": miernik1.setCursorColor(AGaugeNeedleColor.Gray); break;
                case "Zielony": miernik1.setCursorColor(AGaugeNeedleColor.Green); break;
                case "Czerwony": miernik1.setCursorColor(AGaugeNeedleColor.Blue); break;
                case "Fioletowy": miernik1.setCursorColor(AGaugeNeedleColor.Blue); break;
                case "Magenta": miernik1.setCursorColor(AGaugeNeedleColor.Magenta); break;
                default: miernik1.setCursorColor(AGaugeNeedleColor.Blue); break;
            }
            
        }
    }
}
